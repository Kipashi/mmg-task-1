const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', {title: 'Grzegorz Faryna', routes: [
        { title: "Task 1", path: "/task1", description: "Webpage which contains one input field Request to google using query including the text from input Render the result + style h3 tag."},
        { title: "Task 2", path: "/task2", description: "Using API https://jsonplaceholder.typicode.com/ Webpage with Input for user id Display posts for that user limited to 5 posts per user ID Show commetns for these posts Add basic styling for the posts (indent)"}
    ]});
});

module.exports = router;
