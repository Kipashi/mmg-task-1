const express = require('express');
const router = express.Router();
const request = require('request');
const cheerio = require('cheerio');
const iconv = require('iconv-lite');

/* GET users listing. */
router.get('/', function (req, res) {
    const query = req.query.query ? req.query.query : "";
    request({
        method: "GET",
        uri: `http://www.google.com/search?q=${query}`,
        encoding: null
    }, function (error, response, body) {
        const searchResults = [];
        const dom = cheerio.load(iconv.decode(body, "ISO-8859-2"));
        dom("ol > div", "#search").each(function (i, element) {
            const titleElement = dom(element).find("h3");
            const hrefAttr = dom(titleElement).find("a").attr("href");
            if (hrefAttr) {
                const url = hrefAttr.match('http.*');
                searchResults.push({
                    title: titleElement.text(),
                    url: url ? url[0] : "",
                    text: dom(element).find("span").text()
                });
            }
        });
        res.render('task1', {
            query: query,
            searchResults: searchResults
        });
    })
});

module.exports = router;
