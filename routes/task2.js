const express = require('express');
const router = express.Router();
const Rx = require('@reactivex/rxjs');
const RxHttpRequest = require('rx-http-request').RxHttpRequest;

router.get('/', function (req, res) {
    const userId = req.query.userId;
    if (userId) {
        RxHttpRequest.get(`http://jsonplaceholder.typicode.com/posts?userId=${userId}`, {
            json: true
        }).map(postsResponse => Rx.Observable.from(postsResponse.body).take(5).map(function (post) {
            return RxHttpRequest.get(`http://jsonplaceholder.typicode.com/posts/${post.id}/comments`, {
                json: true
            }).map(commentsResponse => {
                post.comments = commentsResponse.body;
                return post;
            });
        }).combineAll()).combineAll().map(combinedPosts => combinedPosts[0]).subscribe(posts => {
            res.render('task2', {posts: posts});
        });
    } else {
        res.render('task2', {posts: []});
    }
});

module.exports = router;
